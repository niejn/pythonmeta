#!/usr/bin/python

class Student(object):
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return 'Student object (name: %s)' % self.name

    __repr__ = __str__


print(Student("Faker"))



class Fib(object):
    def __init__(self):
        self.a, self.b = 0, 1

    def __iter__(self):
        return self

    def next(self):
        self.a, self.b = self.b, self.a + self.b
        if self.a > 100:
            raise StopIteration()

        return self.a

    def __getitem__(self, n):
        a, b = 1, 1
        for x in range(n):
            a, b = b, a + b
        return a


# for n in Fib():
#     print(n)

f = Fib()
print(f[0])

print(f[3])

print(f[5])

print(f[10])

print(f[11])



# MetaClass
class ListMetaclass(type):
    def __new__(cls, name, basses, attrs):
        attrs['add'] = lambda self, value: self.append(value)
        return type.__new__(cls, name, basses, attrs)


class MyList(list):
    meta = ListMetaclass
    __metaclass__ = ListMetaclass


L = MyList()
L.add(1)
print(L)
