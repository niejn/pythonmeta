#!/usr/bin/env python
# -*-coding:utf-8-*-

# author : "qiulimao"
# email  : "qiulimao@getqiu.com"

"""
 the module's duty
"""


# ---------- code begins below -------

class SimpleMetaClass(type):
    def __init__(self, *args, **kwargs):
        print("you have create a class instance by metaclass")

        super(SimpleMetaClass, self).__init__(*args, **kwargs)


class Earth(object, metaclass=SimpleMetaClass):
    # __metaclass__ = SimpleMetaClass

    def sayHello(self):
        print("hello world")



if __name__ == "__main__":
    print("do something that have nothing with SimpleMetaClass and Earth")
