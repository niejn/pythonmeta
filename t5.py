#!/usr/bin/env python  

print('1. Metaclass declaration')



class Meta(type):
    def __init__(cls, name, bases, attrd):
        super(Meta, cls).__init__(name, bases, attrd)
        print('3. Create class %r' % (name))



print('2. Class Foo declaration')



class Foo(object):
    __metaclass__ = Meta

    def __init__(self):
        print('*. Init class %r' % (self.__class__.__name__))



print('4. Class Foo f1 instantiation')

f1 = Foo()

print('5. Class Foo f2 instantiation')

f2 = Foo()

print('END' )
